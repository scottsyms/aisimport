#!/usr/bin/env python3
'''


'''
#import multiprocessing
from pudb.remote import set_trace
import os
import queue
import sys
import time
import json
import gzip
import ais
import psycopg2
import io
from shapely.geometry import Point
from shapely import wkb

from datetime import datetime
#from multiprocessing import Queue
from pathlib import Path

inputFileList = queue.Queue(0)
fileContents = queue.Queue(0)
type5AIS = queue.Queue(0)
parsedAIS = queue.Queue()

sys.exit()
debug = False

# Function to parse the tag block
def explodesentence(line):
    pieces = {}
    line = line.strip()
    if 'PMWTS' in line:
        return
    else:
        line = line.split('\\')
        # Pull apart prefix codes
        tagblock = line[1].split(',')
        for i in line[1].split(','):
            if i[:2] == 'g:':
                pieces['grouprank'] = i[2:3]
                temp = i.split('*')[0]
                pieces['groupcode'] = temp.split('-')[2][-4:]
            if i[:2] == 'c:':
                temp = i.split('*')[0]
                pieces['epoch'] = temp[-10:]
                pieces['reporttime'] = datetime.fromtimestamp(int(pieces['epoch']))
            if i[:2] == 's:':
                temp = i[2:]
                pieces['source'] = temp
        pieces['data'] = line[2]
        return pieces

# Walk through all the subdirectories of the input directory and build a list of the files that must be consumed
def walkFileList(filePathRoot, debug=False):
    # Walk down the file structure place the relative path for matching filenames on the queue stack.
    print("Assembling file list to process.")
    for dirName, subdirList, fileList in os.walk(root):
        prefix = Path(dirName)
        for fname in fileList:
            if "gz" in fname.lower():
                if debug: print(str(prefix) + "/" + str(fname))
                inputFileList.put(prefix / fname)


# Go through the list of candidate files and add each line to a parsing queue.
# tag the line with the source of the file.
def slurpFiles(debug=False):
    if not debug:
        while  inputFileList.qsize() < 1:
            time.sleep(10)
    k = 0
    if debug: print("Grabbing the next file.")
    if debug: print("Files to process: ", inputFileList.qsize())
    if debug: print("Is the queue empty: ", inputFileList.empty())
    while not inputFileList.empty():
        k = k + 1
        if k / 10 == round(k / 10):
            print("Remaining: ", inputFileList.qsize())
        nextfile = inputFileList.get()
        if debug: print("Opening file: ", nextfile)
        # Determine the source from filename
        if "exactearth" in str(nextfile).lower():
            sourcename = "exactearth"
        else:
            sourcename = "maerospace"
        dfile = gzip.open(nextfile, 'r').read().decode()
        dfile = str(dfile)
        i = 0
        if debug: print("Putting the data in the queue.")
        for line in dfile.split('\n'):
            fileContents.put((sourcename, line))

# Generate a dictionary with the parsed AIS data and add it to a new stack of items to be written to file.
# Type5 messages are handed to a separate parsing stack to get around some issues with multiprocessing.
def parseAIS(debug=False, worker_id=0):
    if not debug:
        while fileContents.qsize() < 100000:
            time.sleep(10)

    # Turn on debugging for process 0
    #if worker_id == 0:
    #    set_trace(term_size=(80, 50))
    print("AIS parsing has woken up.")
    if debug: print("Number of elements in the fileContents Queue: ", fileContents.qsize())
    k = 0

    # Cycle through all the captured lines
    while not fileContents.empty():
        k = k +1
        if debug: print("\n******************************************************************")
        if debug: print("File contents queue is not empty: ", fileContents.qsize())
        source, line = fileContents.get()
        if debug: print("Line: ", line)
        if not "AIVDM" in line:
            if debug: print("Whoops! Non-AIVDM line")
            continue
        if debug: print("Source: ", source, " Sentence: ", line)
        line = explodesentence(line)

        if k / 10000 == round(k / 10000):
            print("Remaining: ", fileContents.qsize())
        if "grouprank" in line:
            if line['grouprank'] == '3':
                continue
            if line['grouprank'] == '2':
                reporttime = ""
                source = ""
            else:
                epoch = line['epoch']
                if debug: print("Epoch: ", epoch)
                reporttime = line['reporttime']

            # Is this the first part of a multi-sentence message?
            if debug: print("It's a type 5!")
            if debug: print("I'm putting this on the queue: ", (line['groupcode'], source, reporttime, line['data'], line['grouprank']))
            type5AIS.put((line['groupcode'], source, reporttime, line['data'], line['grouprank']))

        # Is this not a type 5
        elif not "grouprank" in line:
            #continue
            if debug: print("It's not a type 5.")
            if debug: print("The data line: ", line['data'])
            package=line['data'].split(',')[5]
            try:
                msg = ais.decode(package, 0)
                location = Point(msg["x"], msg["y"])
            except:
                msg = {}
                location = None
            if debug: print("Try block: AIS parsed: ", json.dumps(msg))
            if debug and location: print(msg["id"], msg["mmsi"], json.dumps(msg), location.wkb_hex,
                                         line['reporttime'].strftime('%Y-%m-%d %H:%M:%S.%f'), source, line['data'])
            if debug and not location: print("", "", "", "", line['reporttime'].strftime('%Y-%m-%d %H:%M:%S.%f'), source, line['data'])
            if location: parsedAIS.put((msg["id"], msg["mmsi"], json.dumps(msg), location.wkb_hex,
                                        line['reporttime'].strftime('%Y-%m-%d %H:%M:%S.%f'), source, line['data']))
            if not location: parsedAIS.put(("", "", "", "", line['reporttime'].strftime('%Y-%m-%d %H:%M:%S.%f'), source, line['data']))
    print("Exiting AIS parsing: remaining files to process: ", fileContents.qsize())


def parseType5(debug=False):
    if not debug:
        while type5AIS.qsize() < 5000:
            time.sleep(10)
    print("Entering Type 5 handling.")
    type5Cache = {}

    while not (type5AIS.empty() and fileContents.empty()):
        if debug: print("I've entered the type5 handling ")
        key, source, reporttime, line, grouprank = type5AIS.get()
        if debug: print("From Queue: ", key, source, reporttime, line)
        if key in type5Cache and grouprank == '2':
            if debug: print("Stored value: ", type5Cache[key])
            source = type5Cache[key][0]
            reporttime = type5Cache[key][1]
            aispacket1 = type5Cache[key][2].split(',')[len(type5Cache[key][2].split(',')) - 2]
            del type5Cache[key]
            if debug: print("AIS packet 1: ", aispacket1)
            aispacket2 = line.split(',')[len(line.split(',')) - 2]
            if debug: print("AIS packet 2: ", aispacket2)
            try:
                msg = ais.decode(aispacket1 + aispacket2, 2)
                if debug: print("MSG: ", msg)
            except:
                if debug: print("Whoops!  I can't parse something: ", aispacket1 + aispacket2)
                continue

            if debug: print("Putting: ", msg["id"], msg["mmsi"], json.dumps(msg), "", reporttime, source, line)
            parsedAIS.put((msg["id"], msg["mmsi"], json.dumps(msg), "", reporttime, source, line))
            #parsedAIS.put((msg["id"], msg["mmsi"], json.dumps(msg), "", reporttime.strftime('%Y-%m-%d %H:%M:%S.%f'), source, line))
        else:
            if debug: print("Inserting Source: ", source, "Report time: ", reporttime, "Line: ", line)
            type5Cache[key] = (source, reporttime, line)
    print("Exiting Type 5 handling.")


def writetofile(debug=False):
    if not debug:
        while parsedAIS.qsize() < 10000:
            time.sleep(10)
    print("Entering file writing....")

    dumpfilesize = 1000000
    signalsize = 10000
    if debug:
        qrest = 1
    else:
        qrest = 20
    while 1:
        if debug: print("Queue size: ", parsedAIS.qsize())
        dumpfile = ""
        if not parsedAIS.empty():
            if debug: print("Queue is not empty.")
            for i in range(0, parsedAIS.qsize()):
                line = parsedAIS.get()
                dumpfile = dumpfile + "\t".join(map(str, line)) + "\n"
                if i / signalsize == round(i / signalsize):
                    print("Writing: ", i)
                if (i / dumpfilesize == round(i / dumpfilesize) and i > 2) or parsedAIS.empty():
                    print("Adding: ", i, line[:20])
                    filename = str(os.getpid()) + "-" + datetime.now().strftime("%s%f") + ".gz"
                    with gzip.open('/data/output/' + filename, 'wt') as output:
                        print("Writing: ", filename, " left in queue: ", parsedAIS.qsize())
                        output.write(dumpfile)
                        dumpfile = ""
            if debug: print("Dumped file!")
        else:
            return
        time.sleep(qrest)


def cycleStatus(debug=False):
    oldQsize = fileContents.qsize()
    if not debug: time.sleep(30)
    while not (inputFileList.empty() and fileContents.empty() and parsedAIS.empty() and type5AIS.empty()):
        showStatus(debug)
    print("Bailing status.")


def writetodatabase(debug=False):
    if not debug:
        while parsedAIS.qsize() < 100000:
            time.sleep(10)
    datafile = io.StringIO()
    # Local Development Database
    conn = psycopg2.connect(user="postgres", password="postgres", host="localhost", port="5433", database="ais")
    # remote database
    # postgreSQL_pool = psycopg2.pool.SimpleConnectionPool(1, 80, user="postgres", password="postgres", host="192.168.2.124", port="5432", database="ais")
    c = conn.cursor()
    i = 0
    while not parsedAIS.empty():
        datafile.write(parsedAIS.get())
        i = i + 1
        if i > 100000:
            print("a thousand")
            i = 0
            datafile.close()
            datafile = io.StringIO()


def showStatus(debug=False):
    oldQsize = fileContents.qsize()
    if not debug: time.sleep(10)
    print("*********************************************************")
    print("Files to process: ", inputFileList.qsize())
    print("Lines to process: ", fileContents.qsize())
    print("Type 5 messages: ", type5AIS.qsize())
    print("Parsed AIS: ", parsedAIS.qsize())
    if oldQsize == fileContents.qsize() and not fileContents.qsize() == 0:
        print("Plateaued!!")
    print("*********************************************************")


if len(sys.argv) > 1 and sys.argv[1] == 'debug':
    root = "/home/scottsyms/code/aisimport/space2"
    print("Calling walkFileList with ", root)
    walkFileList(root, True)
    showStatus(True)
    print("Calling slurpfiles ")
    slurpFiles(True)
    showStatus(True)
    print("Calling parseAIs ")
    parseAIS(True)
    print("Calling type5Cache ")
    parseType5(True)
    print("Calling writetofile ")
    writetofile(True)

elif len(sys.argv) > 1 and sys.argv[1] == 'serial':
    root = "/run/media/scottsyms/SPACEBASED/ais/2019/q1"
    print("Calling walkFileList with ", root)
    walkFileList(root, False)
    showStatus(False)
    print("Calling slurpfiles ")
    slurpFiles(False)
    showStatus(False)
    print("Calling parseAIs ")
    parseAIS(False)
    print("Calling type5Cache ")
    parseType5(False)
    print("Calling writetofile ")
    writetofile(False)
else:
    root = "/home/scottsyms/code/aisimport/space2"
    processes = []

    # Fire up the components
    # Walk the file tree
    for i in range(1):
        t = multiprocessing.Process(target=walkFileList, args=(root,))
        processes.append(t)
        t.start()

    # Show the status of the processing
    t = multiprocessing.Process(target=cycleStatus)
    processes.append(t)
    t.start()


    # Slurp the files
    for i in range(3):
        t = multiprocessing.Process(target=slurpFiles)
        processes.append(t)
        t.start()

    # ParseAIS
    for i in range(1):
        t = multiprocessing.Process(target=parseAIS, args=(False, i))
        processes.append(t)
        t.start()

    # ParseAIS Type 5
    for i in range(1):
        t = multiprocessing.Process(target=parseType5())
        processes.append(t)
        t.start()

    #  Write to file
    for i in range(8):
        t = multiprocessing.Process(target=writetofile)
        processes.append(t)
        t.start()

    for one_process in processes:
        one_process.join()

