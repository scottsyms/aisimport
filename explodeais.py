#!/usr/bin/python3

import gzip
from datetime import datetime


def explodesentence(line):
    pieces={}
    line=line.decode('utf-8').strip()
    if 'PMWTS' in line:
        return
    else:
        line=line.split('\\')
        # Pull apart prefix codes
        tagblock=line[1].split(',')
        for i in line[1].split(','):
            if i[:2] == 'g:':
                pieces['grouprank'] = i[2:3]
                temp = i.split('*')[0]
                pieces['groupcode'] = temp.split('-')[2][-4:]
            if i[:2] == 'c:':
                temp = i.split('*')[0]
                pieces['epoch'] = temp[-10:]
                pieces['reporttime'] = datetime.fromtimestamp(int(pieces['epoch']))
            if i[:2] == 's:':
                temp = i[2:]
                pieces['source'] = temp
        pieces['data'] = line[2]
        return pieces



f=gzip.open('./space/sample.nmea.gz', 'r').readlines()

for i in f:
    print("\nLine: ", i)
    print(explodesentence(i))

