'''
Script to insert Maerospace and exactEart archive files into sharded PostGIS database

'''

# Import libraries
import gzip
import sys
import ais
import os
import json
import time
from datetime import datetime
from pathlib import Path
from queue import Queue
from threading import Thread

import psycopg2
from psycopg2 import pool
from psycopg2.extras import Json


# Set number of threads to spool and initialize other variables
num_threads = 1
input_elements = Queue(maxsize=0)

# Start timer
start = time.time()


def parseAISfiles():
    # Set line count iterator to 0
    global reporttime
    i = 0
    epoch=""
    aispacket=""

    # Grab a connection from the Postgresql object pool
    conn = postgreSQL_pool.getconn()
    c = conn.cursor()

    # Grab the next file name from the input queue
    while not input_elements.empty():
        nextfile = input_elements.get()
        print("Opening ", nextfile)
        print("Files left to process", input_elements.qsize())

        # Determine the source from filename
        if "exactearth" in str(nextfile).lower():
            sourcename = "exactearth"
        else:
            sourcename = "maerospace"
        print("Source name is: ", sourcename)
        dfile = gzip.open(nextfile, 'r').read().decode()
        dfile = str(dfile)
        i = 0
        newreceiver = ""

        c.execute("BEGIN;")
        currentline=0
        identity_sentence=''
        #totallines=len(dfile.split('\n'))
        # print(line[:11])

        for line in dfile.split('\n'):
            i = i + 1
            if i/10000 == round(i/10000):
                print("Parsing line: ", i)

            # Commit after inserting a number of rows
            if i > 100000:
                c.execute("COMMIT;")
                print("Committed")
                c.execute("BEGIN;")
                i = 0

            # Adjust the line for Maerospace formatted sentences
            if line[0:2] != '\\s' and line[0:2] != '\\g':
                line = line[10:]

            # Validate line
            #print(line)

            # Iterate through the file
            if line[0:4] == "\\g:1":
                epoch = line.split(",")[2].split(":")[1].split("*")[0]

                aispacket = line.split(',')[len(line.split(',')) - 2]
                reporttime = datetime.fromtimestamp(int(epoch))  # to add milliseconds, use "%Y:%m:%d:%H:%M:%S:%f"
                for line in dfile.split('\n'):
                   currentline=currentline+1

            elif line[0:4]=="\\g:2":
                aispacket= aispacket + line.split(',')[len(line.split(','))-2]
                #print("Decoding merged sentence", len(aispacket))
                try:
                    msg = ais.decode(aispacket, 2)
                except:
                    print("parsing error", end = "  ")
                    continue
                c.execute('''insert into aisdata (receive_time, source, ais, sentence_type, data, mmsi)
                    values ( %(received)s, %(sourcename)s, %(ais)s, %(type)s, %(message)s, %(mmsi)s );''',
                          {'received': reporttime, 'sourcename': sourcename, 'ais': line.strip(), 'type': msg['id'], 'message': json.dumps(msg), 'mmsi': msg['mmsi'], 'rowid': id})

            # If it's a time stamp line, pull out the time and the MMSI of the receiver
            elif line[0:2] == "\\s":
                    # The sentence not a type 5
                    i=i+1
                    sentence = line

                    # break off the tag block in the AIS sentence
                    AISbegins=line.find('!AIVDM')
                    tagblock=line[:AISbegins]
                    #print("Prepackage: ", line[AISbegins:])
                    timebegins=tagblock.find('c:')
                    #print("TAG Block: ", tagblock)
                    epoch = line[timebegins+2:timebegins+12]
                    #print("Epoch: ", epoch)
                    #epoch = line.split(',')[1].split(":")[1].split("*")[0]
                    reporttime = datetime.fromtimestamp(int(epoch))  # to add milliseconds, use "%Y:%m:%d:%H:%M:%S:%f"

                    # Isolate the six bit ascii string
                    package=line[AISbegins:]
                    #print("AIS portion: ", package.split(',')[5])
                    package = package.split(',')[5] #line.strip().split(",")[6].ljust(28, '0')
                    #print("Package: ", package)

                    try:
                        msg = ais.decode(package, 0)
                    except:
                        msg={}
                    if 'x' in msg and 'y' in msg:
                        # if there's a lat and long in the data, insert it
                        c.execute( '''insert into aisdata (receive_time, source, ais, sentence_type, data, location, mmsi) 
                        values ( %(received)s, %(sourcename)s, %(ais)s, %(type)s, %(message)s, 
                        'SRID=4326;POINT(%(longitude)s %(latitude)s)', %(mmsi)s );''',
                        { 'received' : reporttime, 'sourcename' : sourcename, 'ais' : line.strip(), 'type' : msg['id'], 'message' : json.dumps(msg), 'longitude' : msg["x"], 'latitude' : msg["y"], 'mmsi' : msg['mmsi'], 'rowid' : id })
                    else:
                        c.execute('''insert into aisdata (receive_time, source, ais, mmsi) values ( %(received)s, %(sourcename)s, %(ais)s, %(mmsi)s );''', { 'received' : reporttime, 'sourcename' : sourcename, 'ais' : line.strip(), 'mmsi' : ""})
        c.execute("COMMIT;")
        print("Commit")
        input_elements.task_done()
    c.close()
    postgreSQL_pool.putconn(conn)


if __name__ == '__main__':
    # Identify the root of the volume you want to parse
    # These first four are the GNM only directories
    # The last is a directory with NM4 files that can be use to verify.
    root = "/home/scottsyms/code/aisimport/space"
    #root = "/space/ais/2012"

    # Walk down the file structure place the relative path for matching filenames on the queue stack.
    print("Assembling file list to process.")
    for dirName, subdirList, fileList in os.walk(root):
        prefix = Path(dirName)
        for fname in fileList:
            if "gz" in fname.lower():
                input_elements.put(prefix / fname)
                print("File: ", fname)
    print("Done.")

    # Local Development Database
    postgreSQL_pool = psycopg2.pool.SimpleConnectionPool(1, 80, user="postgres", password="postgres", host="localhost", port="5433", database="ais")

    # remote database
    #postgreSQL_pool = psycopg2.pool.SimpleConnectionPool(1, 80, user="postgres", password="postgres", host="192.168.2.124", port="5432", database="ais")
    if (postgreSQL_pool): print("Connection pool created successfully")
    conn = postgreSQL_pool.getconn()
    c = conn.cursor()

    # Create a number of threads to parse the files identified by the subdirectory walk
    print("Initiating threads.")
    for i in range(num_threads):
        # Call the parsing of the AIS files.
        print("Starting thread: ", i)
        worker = Thread(target=parseAISfiles)
        worker.setDaemon(True)  # setting threads as "daemon" allows main program to
        worker.start()

    # now we wait until the queue has been processed
    input_elements.join()
    print("Queue is empty...")

    print("Threads: ", num_threads)
    print("Number of files observed: ", input_elements.qsize())

    end = time.time()
    print("Executed in ", end - start, " seconds")
