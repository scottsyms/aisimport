#!/usr/bin/env python3
'''
Dataframes!  Optimising raw database load using the convenience functions
from dataframes.
'''

import pandas as pd
import numpy as np
from pathlib import Path
from bitstring import BitString

# Import GEO spatial libraries
from shapely.geometry import Point
from shapely import wkb

import re
import os
import time

from sqlalchemy import create_engine

def processfile(prefix, filename):
    print(filename, '... ', end = '', flush=true)
    start=time.time()
    df=pd.read_csv(prefix / filename, sep = '!', names = ["prefix", "data"])
    df['received_time']=df.prefix.str.extract(r'(^[\d]*)', flags = re.IGNORECASE, expand=False).fillna('')
    df['station']=df.prefix.str.extract(r'(s:)([\w-]*)', flags = re.IGNORECASE, expand=False)[1].fillna('')
    df['qflag']=df.prefix.str.extract(r'(q:)([\w-]*)', flags = re.IGNORECASE, expand=False)[1].fillna('')
    df['grouping']=df.prefix.str.extract(r'(g:)([\w-]*)', flags = re.IGNORECASE, expand=False)[1].fillna('')
    df['transmission_time']=df.prefix.str.extract(r'(c:)([\d]*)', flags = re.IGNORECASE, expand=False)[1].fillna('')
    df['filesource'] = filename
    #temp = df.data.str.split(',', expand=True)
    #temp.rename(columns={5: 'payload'}, inplace='True')
    #df['payload'] = temp.payload.apply(convertPayload)
    #df['messagetype'] = df.payload.apply(msgtype)
    #df['position'] = df.payload.apply(position)
    print("writing to db ... ", end = '', flush = True)

    df.to_sql('aisarchive', con=engine, if_exists='append')
    print(time.time() - start, " seconds.", flush = True)

def convertPayload(string):
    """
    AIS payloads are encoded in six bit ascii.  This converts the payload into
    ASCII for further processing.
    """
    bit_piece = ''
    for piece in str(string):
        # Subtract 48 from the ascii value.
        # If the value is greater than 40, subtract 8
        ascii_piece = ord(piece) - 48
        if ascii_piece > 40:
                ascii_piece = ascii_piece - 8

        # to convert the string to binary.
        for x in [32, 16, 8, 4, 2, 1]:
                if ascii_piece - x >= 0:
                        ascii_piece = ascii_piece - x
                        bit_piece = bit_piece + '1'
                else:
                        bit_piece = bit_piece + '0'
    return bit_piece

def msgtype(payload):
    return BitString(bin=payload)[0:6].uint

def mmsi(payload):
    msgtype = BitString(bin=payload)[0:6].uint
    if msgtype in [1, 2, 3, 4, 5, 18, 24]:
        return str(BitString(bin=payload)[8:38].uint).zfill(9)
    else:
        return None


def position(payload):
    payload=BitString(bin=payload)
    payload = payload + BitString('0x100000000000000000000000000000000000000000000000000')
    msgtype = payload[0:6].uint
    if msgtype in [1, 2, 3 ]:
        latitude=float(payload[89:116].int)/600000
        longitude=float(payload[61:89].int)/600000
    if msgtype in [4]:
        latitude = float(payload[107:134].int) / 600000
        longitude = float(payload[79:107].int) / 600000
    if msgtype in [18]:
        latitude = float(payload[85:112].int) / 600000
        longitude = float(payload[57:85].int) / 600000
    else:
        return None
    return Point(longitude, latitude).wkb_hex


if __name__ == '__main__':
    # Select a database
    #engine=create_engine('sqlite:////home/scottsyms/code/aisimport/db/test.db', echo = False)
    #engine=create_engine('sqlite://', echo = False)
    engine = create_engine('postgresql://postgres:postgres@localhost:5433/ais', echo=False)

    # Select a root
    root = "/run/media/scottsyms/SPACEBASED/ais/2019/q1"
    #root = "/home/scottsyms/code/aisimport/space"
    # Walk down the file structure place the relative path for matching filenames on the queue stack.
    print("Assembling file list to process.")
    for dirName, subdirList, fileList in os.walk(root):
        prefix = Path(dirName)
        i = 0
        for fname in fileList:
            i = i +1
            if "gz" in fname.lower():
                print("Loading ", i, " of ", len(fileList), " files ... ", end='', flush = True)
                processfile(prefix , fname)
