--playing with creating a column store for ais data

drop foreign table ee;

create server cstore_server foreign data wrapper cstore_fdw;

    CREATE FOREIGN TABLE "ee" (
	"id" SERIAL NOT NULL,
	"data" JSON NULL,
	"receive_time" TIMESTAMP NULL,
	"source" VARCHAR(20) NOT NULL DEFAULT E'0',
	"ais" VARCHAR(128) NULL)
	
	SERVER cstore_server
	OPTIONS(compression 'pglz');

CREATE INDEX ON ee using brin (receive_time);
COMMENT ON COLUMN "ee"."id" IS E'Row ID';
COMMENT ON COLUMN "ee"."data" IS E'Parsed AIS data in GPSD JSON format';
COMMENT ON COLUMN "ee"."receive_time" IS E'Time the AIS sentence was received by the satellite.';
COMMENT ON COLUMN "ee"."ais" IS E'RAW AIS message.';
COMMENT ON COLUMN "ee"."source" IS E'Source of the AIS data.';

