--  Postgresql database to collect and process space based AIS.

--  Switch to new database to delete old ais db
\c postgres
drop database ais;


--  Create database and  connect
create database "ais"
    WITH OWNER "postgres"
    ENCODING 'UTF8'
    LC_COLLATE = 'en_US.UTF-8'
    LC_CTYPE = 'en_US.UTF-8'
    TEMPLATE template0;

\c ais

--  Ensure the Citus extension is loaded
create extension citus;

--  And spatial extension
create extension postgis;

--  And column store
create extension cstore_fdw;


CREATE TABLE "aisdata" (
	"id" BIGSERIAL,
	"sentence_type" int,
	"mmsi" text,
	"data" jsonb,
	"location" geometry (POINT, 4326),
	"receive_time" TIMESTAMP NULL,
	"source" VARCHAR(20) NOT NULL DEFAULT E'0',
	"ais" VARCHAR(128) NULL,
	primary key (id, receive_time, mmsi))
	partition by range(receive_time);

CREATE INDEX ON aisdata using brin (receive_time);
CREATE INDEX ON aisdata using GIST (location);

--  PG 11 does not like unique indexes without unique row id
-- CREATE UNIQUE INDEX ON aisdata(ais);  

--  Create date range partitioned database from 2010 to 2024
create table aisdata_2010_01 partition of aisdata for values from ('2010-01-01 00:00:00') to ('2010-02-01 00:00:00');
create table aisdata_2010_02 partition of aisdata for values from ('2010-02-01 00:00:00') to ('2010-03-01 00:00:00');
create table aisdata_2010_03 partition of aisdata for values from ('2010-03-01 00:00:00') to ('2010-04-01 00:00:00');
create table aisdata_2010_04 partition of aisdata for values from ('2010-04-01 00:00:00') to ('2010-05-01 00:00:00');
create table aisdata_2010_05 partition of aisdata for values from ('2010-05-01 00:00:00') to ('2010-06-01 00:00:00');
create table aisdata_2010_06 partition of aisdata for values from ('2010-06-01 00:00:00') to ('2010-07-01 00:00:00');
create table aisdata_2010_07 partition of aisdata for values from ('2010-07-01 00:00:00') to ('2010-08-01 00:00:00');
create table aisdata_2010_08 partition of aisdata for values from ('2010-08-01 00:00:00') to ('2010-09-01 00:00:00');
create table aisdata_2010_09 partition of aisdata for values from ('2010-09-01 00:00:00') to ('2010-10-01 00:00:00');
create table aisdata_2010_10 partition of aisdata for values from ('2010-10-01 00:00:00') to ('2010-11-01 00:00:00');
create table aisdata_2010_11 partition of aisdata for values from ('2010-11-01 00:00:00') to ('2010-12-01 00:00:00');
create table aisdata_2010_12 partition of aisdata for values from ('2010-12-01 00:00:00') to ('2011-01-01 00:00:00');

create table aisdata_2011_01 partition of aisdata for values from ('2011-01-01 00:00:00') to ('2011-02-01 00:00:00');
create table aisdata_2011_02 partition of aisdata for values from ('2011-02-01 00:00:00') to ('2011-03-01 00:00:00');
create table aisdata_2011_03 partition of aisdata for values from ('2011-03-01 00:00:00') to ('2011-04-01 00:00:00');
create table aisdata_2011_04 partition of aisdata for values from ('2011-04-01 00:00:00') to ('2011-05-01 00:00:00');
create table aisdata_2011_05 partition of aisdata for values from ('2011-05-01 00:00:00') to ('2011-06-01 00:00:00');
create table aisdata_2011_06 partition of aisdata for values from ('2011-06-01 00:00:00') to ('2011-07-01 00:00:00');
create table aisdata_2011_07 partition of aisdata for values from ('2011-07-01 00:00:00') to ('2011-08-01 00:00:00');
create table aisdata_2011_08 partition of aisdata for values from ('2011-08-01 00:00:00') to ('2011-09-01 00:00:00');
create table aisdata_2011_09 partition of aisdata for values from ('2011-09-01 00:00:00') to ('2011-10-01 00:00:00');
create table aisdata_2011_10 partition of aisdata for values from ('2011-10-01 00:00:00') to ('2011-11-01 00:00:00');
create table aisdata_2011_11 partition of aisdata for values from ('2011-11-01 00:00:00') to ('2011-12-01 00:00:00');
create table aisdata_2011_12 partition of aisdata for values from ('2011-12-01 00:00:00') to ('2012-01-01 00:00:00');

create table aisdata_2012_01 partition of aisdata for values from ('2012-01-01 00:00:00') to ('2012-02-01 00:00:00');
create table aisdata_2012_02 partition of aisdata for values from ('2012-02-01 00:00:00') to ('2012-03-01 00:00:00');
create table aisdata_2012_03 partition of aisdata for values from ('2012-03-01 00:00:00') to ('2012-04-01 00:00:00');
create table aisdata_2012_04 partition of aisdata for values from ('2012-04-01 00:00:00') to ('2012-05-01 00:00:00');
create table aisdata_2012_05 partition of aisdata for values from ('2012-05-01 00:00:00') to ('2012-06-01 00:00:00');
create table aisdata_2012_06 partition of aisdata for values from ('2012-06-01 00:00:00') to ('2012-07-01 00:00:00');
create table aisdata_2012_07 partition of aisdata for values from ('2012-07-01 00:00:00') to ('2012-08-01 00:00:00');
create table aisdata_2012_08 partition of aisdata for values from ('2012-08-01 00:00:00') to ('2012-09-01 00:00:00');
create table aisdata_2012_09 partition of aisdata for values from ('2012-09-01 00:00:00') to ('2012-10-01 00:00:00');
create table aisdata_2012_10 partition of aisdata for values from ('2012-10-01 00:00:00') to ('2012-11-01 00:00:00');
create table aisdata_2012_11 partition of aisdata for values from ('2012-11-01 00:00:00') to ('2012-12-01 00:00:00');
create table aisdata_2012_12 partition of aisdata for values from ('2012-12-01 00:00:00') to ('2013-01-01 00:00:00');

create table aisdata_2013_01 partition of aisdata for values from ('2013-01-01 00:00:00') to ('2013-02-01 00:00:00');
create table aisdata_2013_02 partition of aisdata for values from ('2013-02-01 00:00:00') to ('2013-03-01 00:00:00');
create table aisdata_2013_03 partition of aisdata for values from ('2013-03-01 00:00:00') to ('2013-04-01 00:00:00');
create table aisdata_2013_04 partition of aisdata for values from ('2013-04-01 00:00:00') to ('2013-05-01 00:00:00');
create table aisdata_2013_05 partition of aisdata for values from ('2013-05-01 00:00:00') to ('2013-06-01 00:00:00');
create table aisdata_2013_06 partition of aisdata for values from ('2013-06-01 00:00:00') to ('2013-07-01 00:00:00');
create table aisdata_2013_07 partition of aisdata for values from ('2013-07-01 00:00:00') to ('2013-08-01 00:00:00');
create table aisdata_2013_08 partition of aisdata for values from ('2013-08-01 00:00:00') to ('2013-09-01 00:00:00');
create table aisdata_2013_09 partition of aisdata for values from ('2013-09-01 00:00:00') to ('2013-10-01 00:00:00');
create table aisdata_2013_10 partition of aisdata for values from ('2013-10-01 00:00:00') to ('2013-11-01 00:00:00');
create table aisdata_2013_11 partition of aisdata for values from ('2013-11-01 00:00:00') to ('2013-12-01 00:00:00');
create table aisdata_2013_12 partition of aisdata for values from ('2013-12-01 00:00:00') to ('2014-01-01 00:00:00');

create table aisdata_2014_01 partition of aisdata for values from ('2014-01-01 00:00:00') to ('2014-02-01 00:00:00');
create table aisdata_2014_02 partition of aisdata for values from ('2014-02-01 00:00:00') to ('2014-03-01 00:00:00');
create table aisdata_2014_03 partition of aisdata for values from ('2014-03-01 00:00:00') to ('2014-04-01 00:00:00');
create table aisdata_2014_04 partition of aisdata for values from ('2014-04-01 00:00:00') to ('2014-05-01 00:00:00');
create table aisdata_2014_05 partition of aisdata for values from ('2014-05-01 00:00:00') to ('2014-06-01 00:00:00');
create table aisdata_2014_06 partition of aisdata for values from ('2014-06-01 00:00:00') to ('2014-07-01 00:00:00');
create table aisdata_2014_07 partition of aisdata for values from ('2014-07-01 00:00:00') to ('2014-08-01 00:00:00');
create table aisdata_2014_08 partition of aisdata for values from ('2014-08-01 00:00:00') to ('2014-09-01 00:00:00');
create table aisdata_2014_09 partition of aisdata for values from ('2014-09-01 00:00:00') to ('2014-10-01 00:00:00');
create table aisdata_2014_10 partition of aisdata for values from ('2014-10-01 00:00:00') to ('2014-11-01 00:00:00');
create table aisdata_2014_11 partition of aisdata for values from ('2014-11-01 00:00:00') to ('2014-12-01 00:00:00');
create table aisdata_2014_12 partition of aisdata for values from ('2014-12-01 00:00:00') to ('2015-01-01 00:00:00');

create table aisdata_2015_01 partition of aisdata for values from ('2015-01-01 00:00:00') to ('2015-02-01 00:00:00');
create table aisdata_2015_02 partition of aisdata for values from ('2015-02-01 00:00:00') to ('2015-03-01 00:00:00');
create table aisdata_2015_03 partition of aisdata for values from ('2015-03-01 00:00:00') to ('2015-04-01 00:00:00');
create table aisdata_2015_04 partition of aisdata for values from ('2015-04-01 00:00:00') to ('2015-05-01 00:00:00');
create table aisdata_2015_05 partition of aisdata for values from ('2015-05-01 00:00:00') to ('2015-06-01 00:00:00');
create table aisdata_2015_06 partition of aisdata for values from ('2015-06-01 00:00:00') to ('2015-07-01 00:00:00');
create table aisdata_2015_07 partition of aisdata for values from ('2015-07-01 00:00:00') to ('2015-08-01 00:00:00');
create table aisdata_2015_08 partition of aisdata for values from ('2015-08-01 00:00:00') to ('2015-09-01 00:00:00');
create table aisdata_2015_09 partition of aisdata for values from ('2015-09-01 00:00:00') to ('2015-10-01 00:00:00');
create table aisdata_2015_10 partition of aisdata for values from ('2015-10-01 00:00:00') to ('2015-11-01 00:00:00');
create table aisdata_2015_11 partition of aisdata for values from ('2015-11-01 00:00:00') to ('2015-12-01 00:00:00');
create table aisdata_2015_12 partition of aisdata for values from ('2015-12-01 00:00:00') to ('2016-01-01 00:00:00');

create table aisdata_2016_01 partition of aisdata for values from ('2016-01-01 00:00:00') to ('2016-02-01 00:00:00');
create table aisdata_2016_02 partition of aisdata for values from ('2016-02-01 00:00:00') to ('2016-03-01 00:00:00');
create table aisdata_2016_03 partition of aisdata for values from ('2016-03-01 00:00:00') to ('2016-04-01 00:00:00');
create table aisdata_2016_04 partition of aisdata for values from ('2016-04-01 00:00:00') to ('2016-05-01 00:00:00');
create table aisdata_2016_05 partition of aisdata for values from ('2016-05-01 00:00:00') to ('2016-06-01 00:00:00');
create table aisdata_2016_06 partition of aisdata for values from ('2016-06-01 00:00:00') to ('2016-07-01 00:00:00');
create table aisdata_2016_07 partition of aisdata for values from ('2016-07-01 00:00:00') to ('2016-08-01 00:00:00');
create table aisdata_2016_08 partition of aisdata for values from ('2016-08-01 00:00:00') to ('2016-09-01 00:00:00');
create table aisdata_2016_09 partition of aisdata for values from ('2016-09-01 00:00:00') to ('2016-10-01 00:00:00');
create table aisdata_2016_10 partition of aisdata for values from ('2016-10-01 00:00:00') to ('2016-11-01 00:00:00');
create table aisdata_2016_11 partition of aisdata for values from ('2016-11-01 00:00:00') to ('2016-12-01 00:00:00');
create table aisdata_2016_12 partition of aisdata for values from ('2016-12-01 00:00:00') to ('2017-01-01 00:00:00');

create table aisdata_2017_01 partition of aisdata for values from ('2017-01-01 00:00:00') to ('2017-02-01 00:00:00');
create table aisdata_2017_02 partition of aisdata for values from ('2017-02-01 00:00:00') to ('2017-03-01 00:00:00');
create table aisdata_2017_03 partition of aisdata for values from ('2017-03-01 00:00:00') to ('2017-04-01 00:00:00');
create table aisdata_2017_04 partition of aisdata for values from ('2017-04-01 00:00:00') to ('2017-05-01 00:00:00');
create table aisdata_2017_05 partition of aisdata for values from ('2017-05-01 00:00:00') to ('2017-06-01 00:00:00');
create table aisdata_2017_06 partition of aisdata for values from ('2017-06-01 00:00:00') to ('2017-07-01 00:00:00');
create table aisdata_2017_07 partition of aisdata for values from ('2017-07-01 00:00:00') to ('2017-08-01 00:00:00');
create table aisdata_2017_08 partition of aisdata for values from ('2017-08-01 00:00:00') to ('2017-09-01 00:00:00');
create table aisdata_2017_09 partition of aisdata for values from ('2017-09-01 00:00:00') to ('2017-10-01 00:00:00');
create table aisdata_2017_10 partition of aisdata for values from ('2017-10-01 00:00:00') to ('2017-11-01 00:00:00');
create table aisdata_2017_11 partition of aisdata for values from ('2017-11-01 00:00:00') to ('2017-12-01 00:00:00');
create table aisdata_2017_12 partition of aisdata for values from ('2017-12-01 00:00:00') to ('2018-01-01 00:00:00');

create table aisdata_2018_01 partition of aisdata for values from ('2018-01-01 00:00:00') to ('2018-02-01 00:00:00');
create table aisdata_2018_02 partition of aisdata for values from ('2018-02-01 00:00:00') to ('2018-03-01 00:00:00');
create table aisdata_2018_03 partition of aisdata for values from ('2018-03-01 00:00:00') to ('2018-04-01 00:00:00');
create table aisdata_2018_04 partition of aisdata for values from ('2018-04-01 00:00:00') to ('2018-05-01 00:00:00');
create table aisdata_2018_05 partition of aisdata for values from ('2018-05-01 00:00:00') to ('2018-06-01 00:00:00');
create table aisdata_2018_06 partition of aisdata for values from ('2018-06-01 00:00:00') to ('2018-07-01 00:00:00');
create table aisdata_2018_07 partition of aisdata for values from ('2018-07-01 00:00:00') to ('2018-08-01 00:00:00');
create table aisdata_2018_08 partition of aisdata for values from ('2018-08-01 00:00:00') to ('2018-09-01 00:00:00');
create table aisdata_2018_09 partition of aisdata for values from ('2018-09-01 00:00:00') to ('2018-10-01 00:00:00');
create table aisdata_2018_10 partition of aisdata for values from ('2018-10-01 00:00:00') to ('2018-11-01 00:00:00');
create table aisdata_2018_11 partition of aisdata for values from ('2018-11-01 00:00:00') to ('2018-12-01 00:00:00');
create table aisdata_2018_12 partition of aisdata for values from ('2018-12-01 00:00:00') to ('2019-01-01 00:00:00');

create table aisdata_2019_01 partition of aisdata for values from ('2019-01-01 00:00:00') to ('2019-02-01 00:00:00');
create table aisdata_2019_02 partition of aisdata for values from ('2019-02-01 00:00:00') to ('2019-03-01 00:00:00');
create table aisdata_2019_03 partition of aisdata for values from ('2019-03-01 00:00:00') to ('2019-04-01 00:00:00');
create table aisdata_2019_04 partition of aisdata for values from ('2019-04-01 00:00:00') to ('2019-05-01 00:00:00');
create table aisdata_2019_05 partition of aisdata for values from ('2019-05-01 00:00:00') to ('2019-06-01 00:00:00');
create table aisdata_2019_06 partition of aisdata for values from ('2019-06-01 00:00:00') to ('2019-07-01 00:00:00');
create table aisdata_2019_07 partition of aisdata for values from ('2019-07-01 00:00:00') to ('2019-08-01 00:00:00');
create table aisdata_2019_08 partition of aisdata for values from ('2019-08-01 00:00:00') to ('2019-09-01 00:00:00');
create table aisdata_2019_09 partition of aisdata for values from ('2019-09-01 00:00:00') to ('2019-10-01 00:00:00');
create table aisdata_2019_10 partition of aisdata for values from ('2019-10-01 00:00:00') to ('2019-11-01 00:00:00');
create table aisdata_2019_11 partition of aisdata for values from ('2019-11-01 00:00:00') to ('2019-12-01 00:00:00');
create table aisdata_2019_12 partition of aisdata for values from ('2019-12-01 00:00:00') to ('2020-01-01 00:00:00');

create table aisdata_2020_01 partition of aisdata for values from ('2020-01-01 00:00:00') to ('2020-02-01 00:00:00');
create table aisdata_2020_02 partition of aisdata for values from ('2020-02-01 00:00:00') to ('2020-03-01 00:00:00');
create table aisdata_2020_03 partition of aisdata for values from ('2020-03-01 00:00:00') to ('2020-04-01 00:00:00');
create table aisdata_2020_04 partition of aisdata for values from ('2020-04-01 00:00:00') to ('2020-05-01 00:00:00');
create table aisdata_2020_05 partition of aisdata for values from ('2020-05-01 00:00:00') to ('2020-06-01 00:00:00');
create table aisdata_2020_06 partition of aisdata for values from ('2020-06-01 00:00:00') to ('2020-07-01 00:00:00');
create table aisdata_2020_07 partition of aisdata for values from ('2020-07-01 00:00:00') to ('2020-08-01 00:00:00');
create table aisdata_2020_08 partition of aisdata for values from ('2020-08-01 00:00:00') to ('2020-09-01 00:00:00');
create table aisdata_2020_09 partition of aisdata for values from ('2020-09-01 00:00:00') to ('2020-10-01 00:00:00');
create table aisdata_2020_10 partition of aisdata for values from ('2020-10-01 00:00:00') to ('2020-11-01 00:00:00');
create table aisdata_2020_11 partition of aisdata for values from ('2020-11-01 00:00:00') to ('2020-12-01 00:00:00');
create table aisdata_2020_12 partition of aisdata for values from ('2020-12-01 00:00:00') to ('2021-01-01 00:00:00');

create table aisdata_2021_01 partition of aisdata for values from ('2021-01-01 00:00:00') to ('2021-02-01 00:00:00');
create table aisdata_2021_02 partition of aisdata for values from ('2021-02-01 00:00:00') to ('2021-03-01 00:00:00');
create table aisdata_2021_03 partition of aisdata for values from ('2021-03-01 00:00:00') to ('2021-04-01 00:00:00');
create table aisdata_2021_04 partition of aisdata for values from ('2021-04-01 00:00:00') to ('2021-05-01 00:00:00');
create table aisdata_2021_05 partition of aisdata for values from ('2021-05-01 00:00:00') to ('2021-06-01 00:00:00');
create table aisdata_2021_06 partition of aisdata for values from ('2021-06-01 00:00:00') to ('2021-07-01 00:00:00');
create table aisdata_2021_07 partition of aisdata for values from ('2021-07-01 00:00:00') to ('2021-08-01 00:00:00');
create table aisdata_2021_08 partition of aisdata for values from ('2021-08-01 00:00:00') to ('2021-09-01 00:00:00');
create table aisdata_2021_09 partition of aisdata for values from ('2021-09-01 00:00:00') to ('2021-10-01 00:00:00');
create table aisdata_2021_10 partition of aisdata for values from ('2021-10-01 00:00:00') to ('2021-11-01 00:00:00');
create table aisdata_2021_11 partition of aisdata for values from ('2021-11-01 00:00:00') to ('2021-12-01 00:00:00');
create table aisdata_2021_12 partition of aisdata for values from ('2021-12-01 00:00:00') to ('2021-01-01 00:00:00');

create table aisdata_2022_01 partition of aisdata for values from ('2022-01-01 00:00:00') to ('2022-02-01 00:00:00');
create table aisdata_2022_02 partition of aisdata for values from ('2022-02-01 00:00:00') to ('2022-03-01 00:00:00');
create table aisdata_2022_03 partition of aisdata for values from ('2022-03-01 00:00:00') to ('2022-04-01 00:00:00');
create table aisdata_2022_04 partition of aisdata for values from ('2022-04-01 00:00:00') to ('2022-05-01 00:00:00');
create table aisdata_2022_05 partition of aisdata for values from ('2022-05-01 00:00:00') to ('2022-06-01 00:00:00');
create table aisdata_2022_06 partition of aisdata for values from ('2022-06-01 00:00:00') to ('2022-07-01 00:00:00');
create table aisdata_2022_07 partition of aisdata for values from ('2022-07-01 00:00:00') to ('2022-08-01 00:00:00');
create table aisdata_2022_08 partition of aisdata for values from ('2022-08-01 00:00:00') to ('2022-09-01 00:00:00');
create table aisdata_2022_09 partition of aisdata for values from ('2022-09-01 00:00:00') to ('2022-10-01 00:00:00');
create table aisdata_2022_10 partition of aisdata for values from ('2022-10-01 00:00:00') to ('2022-11-01 00:00:00');
create table aisdata_2022_11 partition of aisdata for values from ('2022-11-01 00:00:00') to ('2022-12-01 00:00:00');
create table aisdata_2022_12 partition of aisdata for values from ('2022-12-01 00:00:00') to ('2022-01-01 00:00:00');

create table aisdata_2023_01 partition of aisdata for values from ('2023-01-01 00:00:00') to ('2023-02-01 00:00:00');
create table aisdata_2023_02 partition of aisdata for values from ('2023-02-01 00:00:00') to ('2023-03-01 00:00:00');
create table aisdata_2023_03 partition of aisdata for values from ('2023-03-01 00:00:00') to ('2023-04-01 00:00:00');
create table aisdata_2023_04 partition of aisdata for values from ('2023-04-01 00:00:00') to ('2023-05-01 00:00:00');
create table aisdata_2023_05 partition of aisdata for values from ('2023-05-01 00:00:00') to ('2023-06-01 00:00:00');
create table aisdata_2023_06 partition of aisdata for values from ('2023-06-01 00:00:00') to ('2023-07-01 00:00:00');
create table aisdata_2023_07 partition of aisdata for values from ('2023-07-01 00:00:00') to ('2023-08-01 00:00:00');
create table aisdata_2023_08 partition of aisdata for values from ('2023-08-01 00:00:00') to ('2023-09-01 00:00:00');
create table aisdata_2023_09 partition of aisdata for values from ('2023-09-01 00:00:00') to ('2023-10-01 00:00:00');
create table aisdata_2023_10 partition of aisdata for values from ('2023-10-01 00:00:00') to ('2023-11-01 00:00:00');
create table aisdata_2023_11 partition of aisdata for values from ('2023-11-01 00:00:00') to ('2023-12-01 00:00:00');
create table aisdata_2023_12 partition of aisdata for values from ('2023-12-01 00:00:00') to ('2023-01-01 00:00:00');

create table aisdata_2024_01 partition of aisdata for values from ('2024-01-01 00:00:00') to ('2024-02-01 00:00:00');
create table aisdata_2024_02 partition of aisdata for values from ('2024-02-01 00:00:00') to ('2024-03-01 00:00:00');
create table aisdata_2024_03 partition of aisdata for values from ('2024-03-01 00:00:00') to ('2024-04-01 00:00:00');
create table aisdata_2024_04 partition of aisdata for values from ('2024-04-01 00:00:00') to ('2024-05-01 00:00:00');
create table aisdata_2024_05 partition of aisdata for values from ('2024-05-01 00:00:00') to ('2024-06-01 00:00:00');
create table aisdata_2024_06 partition of aisdata for values from ('2024-06-01 00:00:00') to ('2024-07-01 00:00:00');
create table aisdata_2024_07 partition of aisdata for values from ('2024-07-01 00:00:00') to ('2024-08-01 00:00:00');
create table aisdata_2024_08 partition of aisdata for values from ('2024-08-01 00:00:00') to ('2024-09-01 00:00:00');
create table aisdata_2024_09 partition of aisdata for values from ('2024-09-01 00:00:00') to ('2024-10-01 00:00:00');
create table aisdata_2024_10 partition of aisdata for values from ('2024-10-01 00:00:00') to ('2024-11-01 00:00:00');
create table aisdata_2024_11 partition of aisdata for values from ('2024-11-01 00:00:00') to ('2024-12-01 00:00:00');
create table aisdata_2024_12 partition of aisdata for values from ('2024-12-01 00:00:00') to ('2024-01-01 00:00:00');

create table aisdata_default partition of aisdata default;

--  Distribute the aisdata table across the Citus cluster.
--select create_distributed_table('aisdata', 'mmsi');

--select mmsi, count(*) as reports from aisdata group by mmsi order by reports desc;

--select mmsi, st_makeline(array(select location from aisdata where mmsi='367863000' order by receive_time)) from aisdata where mmsi='367863000';

--dump data from aisdata table
--copy (select * from aisdata) to '/var/lib/postgresql/data/aisdata.txt' with csv header;





